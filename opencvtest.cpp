#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;

int image2[480][640];
int rows = 0;
int cols = 0;

int intensity[480][640];
int direction[480][640];

int sensitivityBar = 10;
int otherBar = 0.22;

Mat test;

int sobelHorizontal(int x, int y, Mat image)
{    
    int kernel[3][3] = {-1,0,1,
                        -2,0,2,
                        -1,0,1};

    /*
    int pixel = image.at<uchar>(x, y);
    
    pixel += image.at<uchar>(x - 1, y) * -2;
    pixel += image.at<uchar>(x - 1, y - 1) * -1;

    pixel += image.at<uchar>(x - 1, y + 1) * -1;
    pixel += image.at<uchar>(x + 1, y - 1) * 1;

    pixel += image.at<uchar>(x + 1, y) * 2;
    pixel += image.at<uchar>(x + 1, y + 1) * 1;
    */

    int pixel = image2[x][y];

    pixel += image2[x-1][y] * -2;
    pixel += image2[x-1][y-1] * -1;

    pixel += image2[x-1][y+1] * -1;
    pixel += image2[x+1][y-1] * 1;

    pixel += image2[x+1][y] * 2;
    pixel += image2[x+1][y+1] * 1;

    pixel /= 6;

    return pixel;
}


int sobelVertical(int x, int y, Mat image)
{
    int kernel[3][3] = {-1,-2,-1,
                        0,0,0,
                        1,2,1};

    /*
    int pixel = image.at<uchar>(x, y);

    pixel += image.at<uchar>(x - 1, y - 1) * -1;
    pixel += image.at<uchar>(x - 1, y + 1) * 1;

    pixel += image.at<uchar>(x, y - 1) * -2;
    pixel += image.at<uchar>(x, y + 1) * 2;

    pixel += image.at<uchar>(x + 1, y - 1) * -1;
    pixel += image.at<uchar>(x + 1, y + 1) * 1;
    */

    int pixel = image2[x][y];

    pixel += image2[x-1][y-1] * -1;
    pixel += image2[x-1][y+1] * 1;

    pixel += image2[x][y-1] * -2;
    pixel += image2[x][y+1] * 2;

    pixel += image2[x+1][y-1] * -1;
    pixel += image2[x+1][y+1] * 1;
    

    pixel /= 6;

    return pixel;
}


void sobel(Mat image)
{
    Mat sobelImage = image.clone();

    for (int i = 2; i < rows - 2; i++)
    {
        for (int j = 2; j < cols - 2; j++)
        {
            int x = sobelHorizontal(i, j, sobelImage);
            int y = sobelVertical(i, j, sobelImage);

            int sum = x^2 + y^2;

           // sum = sum > 255 ? 255:sum;
          // sum = sum < 0 ? 0 : sum;

            sobelImage.at<uchar>(i, j) = sum;            
        }
    }
    imshow("Sobel", sobelImage);
}

void gaussian(Mat image)
{
    /// TODO THIS FUNCTION ISNT RIGHT
    /*int gaussian[5][5] = {{2, 4, 5, 4, 2},
                         {4, 9, 12, 9, 4},
                         {5, 12, 15, 12, 5},
                         {4, 9, 12, 9, 4},
                         {2, 4, 12, 4, 2}};

    for (int i = 2; i < rows - 2; i++)
    {
        for (int j = 2; j < cols - 2; j++)
        {
            int pixel = image.at<uchar>(i, j);

            for (int g1 = -2; g1 <= 2; g1++) {
                for (int g2 = -2; g2 <= 2; g2++) {

                    pixel += image.at<uchar>(i+g1, j+g2) * gaussian[g1][g2];
                }
            }
            pixel /= 159;
            image.at<uchar>(i, j) = pixel;
        }
    }*/

    unsigned int blurpixel=0;
    signed int dx = 0, dy = 0;
    unsigned int pixelweight = 0;
    
    // Define gaussian blur weightings array
    int weighting[5][5] =
    {
    { 2, 4, 5, 4, 2},
    { 4, 9,12, 9, 4},
    { 5,12,15,12, 5},
    { 4, 9,12, 9, 4},
    { 2, 4, 5, 4, 2}
    };
    
    
    // Get each pixel and apply the blur filter
    for (int x = 2; x <= rows - 2; x++) 
    {
        for (int y = 2; y <= cols - 2; y++) {
        
            // Clear blurpixel
            blurpixel = 0;
            
            // +- 2 for each pixel and calculate the weighting
            for (dx = -2; dx <= 2; dx++) 
            {
                for (dy = -2; dy <= 2; dy++) 
                {
                pixelweight = weighting[dx+2][dy+2]; 
                
                // Get pixel
                int pixel = image.at<uchar>(x + dx, y + dy);

                // Apply weighting
                blurpixel = blurpixel + pixel * pixelweight;
                }
            }
        // Write pixel to blur image
        image.at<uchar>(x, y) = (int)(blurpixel / 169);
        }
    }
}


void gradientIntensity(Mat image, float param)
{
    int gx = 0;
    int gy = 0;
    int graddir = 0;
    int grad = 0;

    int pixel[3] = { 0, 0, 0 };

    for (int i = 1; i < rows - 1; ++i)
    {
        for (int j = 1; j < cols - 1; ++j)
        {
            pixel[0] = image.at<uchar>(i, j);
            pixel[1] = image.at<uchar>(i-1, j);
            pixel[2] = image.at<uchar>(i, j-1);

            gx = pixel[0] - pixel[1];
            gy = pixel[0] - pixel[2];

            graddir = (int)(abs(atan2(gy, gx)) + param/10) * 80; // 0.22

            direction[i][j] = graddir;

            grad = (int)sqrt(gx * gx + gy * gy) * 2;

            intensity[i][j] = grad;
        }
    }
}


void supression(Mat image)
{
    

    int sensitivity = sensitivityBar;
    int graddir = 0;

    for (int i = 2; i < rows - 2; ++i)
    {
        for (int j = 2; j < cols - 2; ++j)
        {
            if (intensity[i][j] >= sensitivity)
            {
                graddir = direction[i][j];

                if (graddir == 0) {
                    if (intensity[i][j] >= intensity[i][j-1] && intensity[i][j] >= intensity[i][j+1]) {
                        image.at<uchar>(i, j) = 255;

                        image.at<uchar>(i, j - 1) = 0;
                        image.at<uchar>(i, j + 1) = 0;
                    }
                    else
                    {
                        image.at<uchar>(i, j) = 0;
                    }
                }
                else if (graddir == 80)
                {
                    if (intensity[i][j] >= intensity[i+1][j-1] && intensity[i][j] >= intensity[i-1][j+1]) {
                        image.at<uchar>(i, j) = 255;

                        image.at<uchar>(i + 1, j - 1) = 0;
                        image.at<uchar>(i - 1, j + 1) = 0;
                    }
                    else
                    {
                        image.at<uchar>(i, j) = 0;
                    }
                }
                else if (graddir == 160)
                {
                    if (intensity[i][j] >= intensity[i-1][j] && intensity[i][j] >= intensity[i+1][j]) {
                        image.at<uchar>(i, j) = 255;

                        image.at<uchar>(i-1, j) = 0;
                        image.at<uchar>(i+1, j) = 0;
                    }
                    else
                    {
                        image.at<uchar>(i, j) = 0;
                    }
                }
                else if (graddir == 240)
                {
                    if (intensity[i][j] >= intensity[i-1][j-1] && intensity[i][j] >= intensity[i+1][j+1])
                    {
                        image.at<uchar>(i, j) = 255;

                        image.at<uchar>(i-1, j-1) = 0;
                        image.at<uchar>(i+1, j+1) = 0;
                    }
                    else
                    {
                        image.at<uchar>(i, j) = 0;
                    }
                }
                else
                {
                    image.at<uchar>(i, j) = 0;
                }
            }
        }
    }
}

#include <unistd.h>
void canny(int, void*)
{
    Mat cannyImage = test.clone();

    gaussian(cannyImage);
    gradientIntensity(cannyImage, otherBar);    
    for (int i = 0; i < rows; ++i)
    {
        for (int j = 0; j < cols; ++j)
        {

                cannyImage.at<uchar>(i, j) = 0;
        }
    }
    supression(cannyImage);

    imshow("canny", cannyImage);
}



int main()
{
    Mat image = imread("valve.png",CV_LOAD_IMAGE_GRAYSCALE);

   
    Mat src = image.clone();
    Mat c;
    Canny(src, c, 10, 350);
   
    //imshow("c", src);
    //imshow("C2", c);

    rows = image.rows;
    cols = image.cols;

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            image2[i][j] = image.at<uchar>(i, j);
            direction[i][j] = 0;
            intensity[i][j] = 0;
        }
    }
    test = image.clone();
    //imshow("Original", image);

    //sobel(image);

    namedWindow("canny", CV_WINDOW_AUTOSIZE);
    createTrackbar("canny", "canny", &sensitivityBar, 100, canny);
    createTrackbar("canny2", "canny", &otherBar, 100, canny);

    canny(0, 0);




    waitKey(0);

    return 0;
}